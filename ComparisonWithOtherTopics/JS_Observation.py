import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
import time
from numpy import int32
from datetime import datetime
import math

data_sheet_JS = pd.read_csv('DataSheets/JavaScript.csv', encoding='latin-1')
#print(data_sheet_Questions)

#nc=Number Counts of Answers
nc=data_sheet_JS.AnswerCount
print(max(nc))


totalAnswer=0
for i in range(len(nc)):
    if nc[i]>0:
        totalAnswer+=nc[i]

highestANswer = max(nc)
print(highestANswer)
#AvgAnswersPerQuestions=totalAnswer/len(data_sheet_JS.Id)
#print("Avg no of Answers Per Questions")
#print(AvgAnswersPerQuestions)

AnswerCount = [0]*51
for i in range(len(nc)):
    for j in range(0,highestANswer):
        if (nc[i]<=50):
            if (nc[i]==j):
                AnswerCount[j]+=1
        else:
            AnswerCount[50]+= 1


print("Number of  Answers {}".format(totalAnswer))
print("No. of Answers \t\t No. of Posts")
for i in range(len(AnswerCount)):
    print("{} \t\t\t\t\t\t {}".format(i, AnswerCount[i]))


'''

No_of_Answers=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10 or more']
y_pos=np.arange(len(No_of_Answers))
plt.bar(y_pos, AnswerCount)
plt.xticks(y_pos, No_of_Answers)

plt.ylabel('#Posts')
plt.xlabel('#Answers')
plt.title("Posts by No of Answers")
plt.show()
'''
