import pandas as pd
import operator


def dictWithID(Id,x):
    dict = {}
    for i in range(len(Id)):
        key = Id[i]
        dict[key] = x[i]
    return dict


data_sheet_Questions = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Questions.csv', encoding='latin-1')
Id = data_sheet_Questions.Id

CommentCount = data_sheet_Questions.CommentCount
dictComment=dictWithID(Id,CommentCount)
sorted_Comment = sorted(dictComment.items(), key=operator.itemgetter(1), reverse=True)
#print(len(sorted_Comment))

UpVote = data_sheet_Questions.FavoriteCount
UpVote = UpVote.fillna(0)
dictUpVote=dictWithID(Id,UpVote)
sorted_UpVote = sorted(dictUpVote.items(), key=operator.itemgetter(1),reverse=True)
#print(sorted_UpVote[1])



iD = []
for i in range(0,200):
    k = sorted_UpVote[i][0]
    iD.append(k)
print(len(iD))
x = data_sheet_Questions.loc[data_sheet_Questions['Id'].isin(iD)]
x.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Top200Posts_FAVCount.csv')

iD_Comment = []
for i in range(0,200):
    k = sorted_Comment[i][0]

    iD_Comment.append(k)
print(len(iD_Comment))
x = data_sheet_Questions.loc[data_sheet_Questions['Id'].isin(iD_Comment)]
x.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Top200Posts_CommentCount.csv')

df =pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Question.csv', encoding='latin-1')
Length = df.WordCount_TitleTextBody
dictLength = dictWithID(Id,Length)
sorted_Length = sorted(dictLength.items(), key=operator.itemgetter(1),reverse=True)
#print(sorted_Length[:10])
n=0
iD_Length = []
for i in range(0,200):
    k = sorted_Length[i][0]
    iD_Length.append(k)
print(len(iD_Length))
x = data_sheet_Questions.loc[data_sheet_Questions['Id'].isin(iD_Length)]
x.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Top200Posts_Length.csv')


