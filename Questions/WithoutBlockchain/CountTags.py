import pandas as pd
import csv
import time
import numpy as np
from numpy import int32
from datetime import datetime
import operator

data_sheet_Questions = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Questions.csv', encoding='latin-1')
data_sheet_Answers = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Answers.csv', encoding='latin-1')
data_sheet_Comments = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Comments.csv', encoding='latin-1')

#print(data_sheet_Questions)

Question_count = data_sheet_Questions.shape[0]
Answer_count = data_sheet_Answers.shape[0]
Comment_count = data_sheet_Comments.shape[0]

print("No of Questions in StackOverflow with tags (Smartcontract,Ethereum & Solidity) = "+format(Question_count))

print("\nNo of Answers in StackOverflow with tags (Smartcontract,Ethereum & Solidity) = "+format(Answer_count))

print("\nNo of Comments in StackOverflow with tags (Smartcontract,Ethereum & Solidity) = "+format(Comment_count))


Field_name_QuestionsAnswers='\n-->'.join(data_sheet_Questions.columns)

print("\nFields for Questions.csv & Answers.csv\n\n'"+Field_name_QuestionsAnswers)


Field_name_Comments='\'\n\''.join(data_sheet_Comments.columns)

print("\nFields for Comments.csv\n\n-->"+Field_name_Comments)

#Tag Count in Questions

TagsInQuestions_list = data_sheet_Questions.Tags
TagsInQuestions_list = [s.replace('<', '') for s in TagsInQuestions_list]
TagsInQuestions_list = [s.replace('>', ',') for s in TagsInQuestions_list]
#print(TagsInQuestions_list)

TagsInQuestions_str=''.join(TagsInQuestions_list)
print(TagsInQuestions_str)
Tags_list = TagsInQuestions_str.split(",")
print(Tags_list)
Frequency = dict((i, Tags_list.count(i)) for i in Tags_list)

sorted_Frequency = sorted(Frequency.items(), key=operator.itemgetter(1),reverse=True)
#print(sorted_Frequency)


df = pd.DataFrame(sorted_Frequency, columns=['Tags in Questions','Frequency'])
df.to_csv("../../DataSheets/WithoutBlockchain/Observation/TagsFrequency_Questions.csv")

#Tag Count in Answers

TagsInAnswers_list = data_sheet_Questions.Tags
TagsInAnswers_list = [s.replace('<', '') for s in TagsInAnswers_list]
TagsInAnswers_list = [s.replace('>', ',') for s in TagsInAnswers_list]
#print(TagsInQuestions_list)

TagsInAnswers_str=''.join(TagsInAnswers_list)

Tags_list = TagsInAnswers_str.split(",")
#print(Tags_list)
Frequency = dict((i, Tags_list.count(i)) for i in Tags_list)
sorted_Frequency = sorted(Frequency.items(), key=operator.itemgetter(1),reverse=True)

#print(type(sorted_Frequency))

df = pd.DataFrame(sorted_Frequency, columns=['Tags in Answers','Frequency'])
df.to_csv("../../DataSheets/WithoutBlockchain/Observation/TagsFrequency_Answers.csv")







