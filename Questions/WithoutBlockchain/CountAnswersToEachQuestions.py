import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
import time
from numpy import int32
from datetime import datetime
import math

data_sheet_Questions = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Questions.csv', encoding='latin-1')
#print(data_sheet_Questions)

#nc=Number Counts of Answers
nc=data_sheet_Questions.AnswerCount

totalAnswer=0
for i in range(len(nc)):
    if nc[i]>0:
        totalAnswer+=nc[i]

highestANswer = max(nc)
print(highestANswer)
AvgAnswersPerQuestions=totalAnswer/len(data_sheet_Questions.Id)
print("Avg no of Answers Per Questions")
print(AvgAnswersPerQuestions)

AnswerCount = [0]*11
for i in range(len(nc)):
    for j in range(0,highestANswer):
        if (nc[i]<10):
            if (nc[i]==j):
                AnswerCount[j]+=1
        else:
            AnswerCount[10]+= 1


print("Number of  Answers {}".format(totalAnswer))
print("No. of Answers \t\t No. of Posts")
for i in range(len(AnswerCount)):
    print("{} \t\t\t\t\t\t {}".format(i, AnswerCount[i]))


No_of_Answers=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10 or more']
y_pos=np.arange(len(No_of_Answers))
plt.bar(y_pos, AnswerCount)
plt.xticks(y_pos, No_of_Answers)

plt.ylabel('#Posts')
plt.xlabel('#Answers')
plt.title("Posts by No of Answers")
plt.show()
