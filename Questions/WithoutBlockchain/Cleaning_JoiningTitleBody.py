import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt

data_sheet_Questions = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Questions.csv', encoding='latin-1')

QuestionBody_list = data_sheet_Questions.Body
QuestionTitle_list = data_sheet_Questions.Title
#print(QuestionTitle_list)

#Removing Code Snippets
def cleancode(i):
    clean = re.compile('<code>(.|\n|\r\n)*?</code>')
    print(type(clean))
    cleanCode = re.sub(clean, '', i)
    return cleanCode

#Removing HTML Tags
def cleanhtml(X):
    cleanr = re.compile('<.*?>')
    cleanTag = re.sub(cleanr, '', X)

    return cleanTag

QuestionBody_listX=[]

for i in QuestionBody_list:
    X=cleancode(i)
    Y=cleanhtml(X)
    Z=Y.replace('\n',' ')
    W = Z.replace('\r', ' ')
    QuestionBody_listX.append(W)
#print(QuestionBody_listX)

QuestionTitle_listX=[]

for i in QuestionTitle_list:
    X=cleancode(i)
    Y=cleanhtml(X)
    Z=Y.replace('\n',' ')
    W = Z.replace('\r', ' ')
    QuestionTitle_listX.append(W)


print(QuestionTitle_listX)

TagsInQuestions_list = data_sheet_Questions.Tags
print(type(TagsInQuestions_list))

for i in range(len(TagsInQuestions_list)):
    # print(i)
    TagsInQuestions_list[i] = TagsInQuestions_list[i].replace('<', '')
    TagsInQuestions_list[i] = TagsInQuestions_list[i].replace('>', ',')
    # print(TagsInQuestions_list[i])

print(TagsInQuestions_list)


title_Body_Question=[]
for each in range(0,len(QuestionBody_listX)):
    title_Body_Question.append(QuestionTitle_listX[each] + ' '+str(QuestionBody_listX[each])+ ' '+TagsInQuestions_list[each])

print(title_Body_Question)



#for i in QuestionBody_listX:
 #   print ("***********")
  #  print(i)
Word_count_Questions=[]
for i in title_Body_Question:
    count = len(re.findall(r'\w+', i))
    Word_count_Questions.append(count)

#print(Word_count_Questions)
data_sheet_Questions['Cleaned_TitleTextBody'] = title_Body_Question
data_sheet_Questions['WordCount_TitleTextBody'] = Word_count_Questions
data_sheet_Questions.to_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Question.csv')

'''
Word_count_Questions=sorted(Word_count_Questions,reverse=True)
#print(type(Word_count_Questions))
#print(Word_count_Questions)

Counter = {x:Word_count_Questions.count(x) for x in Word_count_Questions}

Counter = Counter.items()
data_sheet_Questions = pd.DataFrame(Counter, columns=['No_of_Words_in_Questions','No_of_Posts'])
data_sheet_Questions.to_csv("../../DataSheets/WithoutBlockchain/Observation/WordCount_Questions.csv")
'''

WC = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Question.csv')
#print(WC)


#Plot

count=WC.WordCount_TitleTextBody
#print(count)

sizes=[0]*12


for i in range(len(count)):
    if (count[i]<=100):
        sizes[1] += 1
    if (count[i]>100 and not(count[i]>200)):
        sizes[2] += 1
    if (count[i]>200 and not(count[i]>300)):
        sizes[3] += 1
    if (count[i]>300 and not (count[i]>400)):
        sizes[4] += 1
    if (count[i]>400 and not (count[i]>500)):
        sizes[5] += 1
    if (count[i]>500 and not (count[i]>600)):
        sizes[6] += 1
    if (count[i]>600 and not (count[i]>700)):
        sizes[7] += 1
    if (count[i]>700 and not (count[i]>800)):
        sizes[8] += 1
    if (count[i]>800 and not (count[i]>900)):
        sizes[9] += 1
    if (count[i]>900 and not (count[i]>1000)):
        sizes[10] += 1
    if (count[i]> 1000):
        sizes[11] += 1

print(sizes)

No_of_Words =['Less than 100', '100-199', '200-299', '300-399', '400-499', '500-599', '600-699', '700-799', '800-899', '900-999', '1000 or more']
y_pos=np.arange(len(No_of_Words))
plt.bar(y_pos, sizes[1:])
plt.xticks(y_pos, No_of_Words)
plt.xlabel('No of Words in Questions')
plt.ylabel('No of Posts')
plt.title("#Word Count with # Questions")
plt.show()



## Verifying No of Posts
'''
XX=pd.read_csv('DataSheets/CleanedDataset_Question.csv',encoding='latin1')
Question_count = XX.shape[0]
print(Question_count)


y =len(WC)
P=0
for i in range(len(y)):
    if y[i]>0:
        P+=y[i]
print(P)
'''