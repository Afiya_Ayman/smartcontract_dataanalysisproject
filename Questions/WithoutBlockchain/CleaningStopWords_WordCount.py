import nltk
from nltk import PorterStemmer
from nltk import SnowballStemmer
from nltk import LancasterStemmer
import pandas as pd
import re

nltk.download('stopwords')
from nltk.corpus import stopwords

port = PorterStemmer()
snowball = SnowballStemmer("english")
lancaster = LancasterStemmer()
df = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Stemming_Questions.csv')  # read in the .csv file

sw_set = set(stopwords.words('english'))

#print("stop words:\n {}")
#print(sw_set)

# df = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Question.csv')
#
# text_col = df['Cleaned_TitleTextBody']
#
# text_tokenize = text_col.apply(nltk.word_tokenize)
# print(text_tokenize[4])


def clean_tokenized(col_name, new_col_name, stemming_method):
    df[new_col_name] = df[col_name].apply(nltk.word_tokenize)

    df[new_col_name] = df[new_col_name].apply(lambda x: [w.lower() for w in x if w.isalpha()])

    df[new_col_name] = df[new_col_name].apply(lambda x: [w for w in x if not w in sw_set])

    df[new_col_name] = df[new_col_name].apply(lambda x: [stemming_method(w) for w in x])


clean_tokenized('Cleaned_TitleTextBody', 'CleanStopWords_PorterStemming_TextBody', port.stem)
clean_tokenized('Cleaned_TitleTextBody', 'CleanStopWords_SnowballStemming_TextBody', snowball.stem)
clean_tokenized('Cleaned_TitleTextBody', 'CleanStopWords_LancasterStemming_TextBody', lancaster.stem)




print( df['CleanStopWords_PorterStemming_TextBody'][1])
print(df['CleanStopWords_SnowballStemming_TextBody'][1])
print(df['CleanStopWords_LancasterStemming_TextBody'][1])

df.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Question_AfterCleaning_Tags_Stemming_StopWords.csv')

WC = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Question_AfterCleaning_Tags_Stemming_StopWords.csv')
P_Stemming = WC.CleanStopWords_PorterStemming_TextBody
SB_Stemming =WC.CleanStopWords_SnowballStemming_TextBody
L_Stemming = WC.CleanStopWords_LancasterStemming_TextBody
#print(P_Stemming)

Word_count_PStemming=[]
for i in P_Stemming:
    count = len(re.findall(r'\w+', i))
    Word_count_PStemming.append(count)

#print(Word_count_PStemming)

Word_count_SBStemming=[]
for i in SB_Stemming:
    count = len(re.findall(r'\w+', i))
    Word_count_SBStemming.append(count)

#print(Word_count_SBStemming)

Word_count_LStemming=[]
for i in L_Stemming:
    count = len(re.findall(r'\w+', i))
    Word_count_LStemming.append(count)

#print(Word_count_LStemming)

WC['Word_count_PStemming'] = Word_count_PStemming
WC['Word_count_SBStemming'] = Word_count_SBStemming
WC['Word_count_LStemming'] = Word_count_LStemming
WC.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Question_AfterCleaning_Tags_Stemming_StopWords.csv')




