import nltk
from nltk import PorterStemmer
import pandas as pd
from nltk.corpus import stopwords
import gensim
from collections import Counter

df = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/Stemming_Questions.csv')
#print(df['CleanStopWords_PorterStemming_TextBody'])

port = PorterStemmer()

sw_set = set(stopwords.words('english'))

# #
# # print("stop words:\n {}")
# # print(sw_set)


def lower_isalpha(row):
    return [w.lower() for w in row if w.isalpha() and len(w) > 2 and len(w) < 100]


def clean_tokenized(col_name, new_col_name, stemming_method):
    df[new_col_name] = df[col_name].apply(nltk.word_tokenize)

    df[new_col_name] = df[new_col_name].apply(lower_isalpha)

    df[new_col_name] = df[new_col_name].apply(lambda x: [w for w in x if not w in sw_set])

    df[new_col_name] = df[new_col_name].apply(lambda x: [stemming_method(w) for w in x])

    df[new_col_name + '_word_count'] = df[new_col_name].apply(lambda x: dict(Counter(x)))


clean_tokenized('Cleaned_TitleTextBody', 'Clean_PorterStemming_TextBody', port.stem)
# clean_tokenized('Cleaned_TextBody', 'Clean_PorterStemming_TextBody', port.stem)
# clean_tokenized('Cleaned_TextBody', 'Clean_PorterStemming_TextBody', port.stem)

word_dict = gensim.corpora.Dictionary(df['Clean_PorterStemming_TextBody'])

print(word_dict)


count = 0
for k, v in word_dict.items():
    print(k, v)
    count += 1
    if count >= 5:
        break

word_dict.filter_extremes(no_below=15, no_above=0.3)

count = 0
for k, v in word_dict.items():
    print(k, v)
    count += 1
    if count >= 5:
        break

bow_corpus = [word_dict.doc2bow(doc) for doc in df['Clean_PorterStemming_TextBody']]

from gensim import corpora, models

tfidf = models.TfidfModel(bow_corpus)
corpus_tfidf = tfidf[bow_corpus]


lda_model = gensim.models.LdaModel(bow_corpus, num_topics=5, id2word=word_dict, passes=100, iterations=50000)

for idx, topic in lda_model.print_topics(-1):
    print('Topic: {} ---- Words: {}'.format(idx, topic))

print('-' * 100)
exit(0)
lda_model_tfidf = gensim.models.LdaModel(corpus_tfidf, num_topics=5, id2word=word_dict)
for idx, topic in lda_model_tfidf.print_topics(-1):
    print('Topic: {} Word: {}'.format(idx, topic))