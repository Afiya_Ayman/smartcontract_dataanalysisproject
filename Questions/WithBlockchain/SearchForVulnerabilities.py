import pandas as pd
import string

data_sheet_Questions = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/CleanedDataset_Question.csv', encoding='latin-1')
data_sheet_Answers = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/Answers_IncludingBlockchain.csv', encoding='latin-1')


#AnswerBody = data_sheet_Answers.Body
#AnswerBody = list(AnswerBody)

QuestionBody=data_sheet_Questions.Cleaned_TitleTextBody

#QuestionBody=data_sheet_Questions.Body
QuestionBody=list(QuestionBody)
print(len(QuestionBody))

def find_substring(needle, haystack):
    index = haystack.find(needle)
    if index == -1:
        return False
    if index != 0 and haystack[index-1] not in string.whitespace:
        return False
    L = index + len(needle)
    if L < len(haystack) and haystack[L] not in string.whitespace:
        return False
    return True

Vulnerability = ['Fallback','fallout','Token', 'King', 'Reentrancy','Re-entrancy',
                 'RaceCondition','Rubixi', 'GiftBox','KingOfTheEtherThrone','WalletLibrary',
                 'Callstack Depth','Assertion Failure','Timestamp Dependency','Parity Multigeniture',
                 'Transaction-Ordering Dependence', 'DoS', 'revert', 'Block Gas Limit',
                 'Timestamp','Transaction-Ordering','Transaction Ordering']


for i in range(len(Vulnerability)):
    Vulnerability[i] = Vulnerability[i].lower()

#print(Vulnerability)

FrequencyDict = {}

for i in range(len(Vulnerability)):
    F = 0
    for j in range(len(QuestionBody)):
        QuestionBody[j] = QuestionBody[j].lower()
        X = find_substring(Vulnerability[i],QuestionBody[j])

        if X == True:
            F+=1

            key = Vulnerability[i]
            # print("\n\n############\n")
            # print(key)
            # print(QuestionBody[j])
            FrequencyDict[key] = F


print(FrequencyDict)
