import pandas as pd
import string

data_sheet_Questions = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/CleanedDataset_Question.csv', encoding='latin-1')
data_sheet_Answers = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/Answers_IncludingBlockchain.csv', encoding='latin-1')


# AnswerBody = data_sheet_Answers.Body
# AnswerBody = list(AnswerBody)

QuestionBody=data_sheet_Questions.Cleaned_TitleTextBody

#QuestionBody=data_sheet_Questions.Body
QuestionBody=list(QuestionBody)
#print((QuestionBody[2]))

# Frameworks = ['Bitcoin','Ethereum','Counterparty','Stellar','Monax','Lisk']
SecurityTools = ['Oyente', 'Mythril','Securify','SmartCheck','ContractLarva','EthIR',
              'MAIAN','Vandal','Rattle','Ethereum Virtual Machine','EVM']

for i in range(len(SecurityTools)):
    SecurityTools[i] = SecurityTools[i].lower()

#print(SecurityTools)



FrequencyDict = {}

for i in range(len(SecurityTools)):
    F = 0
    for j in range(len(QuestionBody)):
        QuestionBody[j] = QuestionBody[j].lower()
        if SecurityTools[i] in QuestionBody[j]:
            F+=1
            key = SecurityTools[i]
            # print("\n\n############\n")
            # print(key)
            # print(QuestionBody[j])
            FrequencyDict[key] = F


print("\nSearch as Sub-String")
print(FrequencyDict)

def find_substring(needle, haystack):
    index = haystack.find(needle)
    if index == -1:
        return False
    if index != 0 and haystack[index-1] not in string.whitespace:
        return False
    L = index + len(needle)
    if L < len(haystack) and haystack[L] not in string.whitespace:
        return False
    return True

FrequencyDict={}
for i in range(len(SecurityTools)):
    F = 0
    for j in range(len(QuestionBody)):
        QuestionBody[j] = QuestionBody[j].lower()
        X = find_substring(SecurityTools[i],QuestionBody[j])

        if X == True:
            F+=1

            key = SecurityTools[i]
            # print("\n\n############\n")
            # print(key)
            # print(QuestionBody[j])
            FrequencyDict[key] = F

print("\nSearch as Whole String")
print(FrequencyDict)

