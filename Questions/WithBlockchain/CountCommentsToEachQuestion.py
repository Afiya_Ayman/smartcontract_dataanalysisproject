import pandas as pd
from matplotlib import pyplot as plt
import numpy as np


data_sheet_Questions = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/Questions_IncludingBlockchain.csv', encoding='latin-1')


#nc=Number Counts of Answers
nc=data_sheet_Questions.CommentCount

totalComment=0
for i in range(len(nc)):
    if nc[i]>0:
        totalComment+=nc[i]

highestComment =max(nc)
print(highestComment)
AvgCommentPerQuestions=totalComment/len(data_sheet_Questions.Id)
print("Avg no of Comments Per Questions")
print(AvgCommentPerQuestions)

CommentCount = [0] * 11

for i in range(len(nc)):
    for j in range(0,highestComment):
        if (nc[i]<10):
            if (nc[i]==j):
                CommentCount[j]+=1
        else:
            CommentCount[10]+= 1


print("Number of  Comments {}".format(totalComment))
print("No. of Comments \t\t No. of Posts")
for i in range(len(CommentCount)):
    print("{} \t\t\t\t\t\t {}".format(i, CommentCount[i]))

No_of_Comments=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10 or more']
y_pos=np.arange(len(No_of_Comments))
plt.bar(y_pos, CommentCount)
plt.xticks(y_pos, No_of_Comments)


plt.ylabel('#Questions')
plt.xlabel('#Comments')
plt.title("Posts by No of Comments")
plt.show()
