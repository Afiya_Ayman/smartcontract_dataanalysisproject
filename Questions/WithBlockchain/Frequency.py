import pandas as pd
import nltk
import string
from nltk.corpus import stopwords


data_sheet_Questions = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/CleanedDataset_Question.csv', encoding='latin-1')
#data_sheet_Questions = pd.read_csv('CleanedDataset_Question.csv', encoding='latin-1')

QuestionBody=data_sheet_Questions.Cleaned_TitleTextBody
print(QuestionBody[:5])
QuestionBody=list(QuestionBody[:3])
print((QuestionBody))





stopwords = list(stopwords.words('english'))
for i in range(len(stopwords)):
    stopwords.append(stopwords[i].capitalize())
# print(stopwords)

QuestionBodyX=[]


for i in range(len(QuestionBody)):
    QuestionBodyX.append(QuestionBody[i].split())

print(len(QuestionBodyX))


result=' '

for word in QuestionBodyX:
    result = result.join(word)

#print(result)

rest = []
for word in result.split():
    if word not in stopwords:
        rest.append(word)
Questions_WithoutStopwords=' '.join(rest)

words = nltk.word_tokenize(Questions_WithoutStopwords)

# x = [''.join(c for c in s if c not in string.punctuation) for s in words]   #Punctuation
# x = [s for s in x if s]                                                     #Empty Strings
# print(len(x))

iterated = 0
removed = 0
for w in words:
    iterated += 1
    if len(w) == 1 or len(w) == 2:
        words.remove(w)
        removed += 1

print(words)


#from collections import Counter
import operator
#Counter(x)
#print(Counter)

FrequencyDict = {}

FrequencyDict=dict((i,words.count(i)) for i in set(words))
#print(FrequencyDict)

FrequencyDict = sorted(FrequencyDict.items(), key=operator.itemgetter(1),reverse=True)
print(FrequencyDict)



df = pd.DataFrame.from_dict(FrequencyDict, orient="index")
#df.to_csv('WordFrequency_FromQuestions.csv')
df.to_csv("../../DataSheets/WithBlockchain/Observation/WordFrequency_FromQuestions.csv")