import nltk
import csv
from nltk import PorterStemmer
from nltk import SnowballStemmer
from nltk import LancasterStemmer
from nltk import punkt
from collections import defaultdict
import pandas as pd

port = PorterStemmer()

data_sheet_Questions = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/CleanedDataset_Question.csv')

text_col = data_sheet_Questions['Cleaned_TitleTextBody']

text_tokenize = text_col.apply(nltk.word_tokenize)

text_stem = text_tokenize.apply(lambda x: [port.stem(word) for word in x])
#print(text_stem)
data_sheet_Questions['PorterStemming_TextBody'] = text_stem
data_sheet_Questions.to_csv('../../DataSheets/WithBlockchain/Dataset/Stemming_Questions.csv')

port = SnowballStemmer("english")

text_stem = text_tokenize.apply(lambda x: [port.stem(word) for word in x])
#print(text_stem)
data_sheet_Questions['SnowballStemming_TextBody'] = text_stem
data_sheet_Questions.to_csv('../../DataSheets/WithBlockchain/Dataset/Stemming_Questions.csv')

port = LancasterStemmer()
text_stem = text_tokenize.apply(lambda x: [port.stem(word) for word in x])
#print(text_stem)
data_sheet_Questions['LancasterStemming_TextBody'] = text_stem
data_sheet_Questions.to_csv('../../DataSheets/WithBlockchain/Dataset/Stemming_Questions.csv')