				2019

Number of total Questions in 2019 is = 411

 Month    No of Questions
------ 	 -------------
1 				 140
2 				 155
3 				 116
4 				 0
5 				 0
6 				 0
7 				 0
8 				 0
9 				 0
10 				 0
11 				 0
12 				 0


				2018

Number of total Questions in 2018 is = 1895

 Month    No of Questions
------ 	 -------------
1 				 174
2 				 151
3 				 161
4 				 143
5 				 202
6 				 168
7 				 177
8 				 172
9 				 126
10 				 134
11 				 152
12 				 135


				2017

Number of total Questions in 2017 is = 588

 Month    No of Questions
------ 	 -------------
1 				 11
2 				 18
3 				 51
4 				 35
5 				 34
6 				 41
7 				 54
8 				 58
9 				 61
10 				 41
11 				 95
12 				 89


				2016

Number of total Questions in 2016 is = 156

 Month    No of Questions
------ 	 -------------
1 				 7
2 				 12
3 				 7
4 				 11
5 				 9
6 				 18
7 				 17
8 				 14
9 				 10
10 				 18
11 				 22
12 				 11


				2015

Number of total Questions in 2015 is = 39

 Month    No of Questions
------ 	 -------------
1 				 0
2 				 0
3 				 0
4 				 0
5 				 1
6 				 0
7 				 0
8 				 11
9 				 2
10 				 5
11 				 12
12 				 8