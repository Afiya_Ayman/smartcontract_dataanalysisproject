import pandas as pd
import numpy as np
from matplotlib import pyplot as plt


df_Users = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Users.csv', encoding='latin-1')
print(len(df_Users))
data_sheet_Users = df_Users.drop_duplicates()
print(len(data_sheet_Users))
#print(data_sheet_Users)
data_sheet_Users.to_csv('../../DataSheets/WithoutBlockchain/Dataset/UniqueUsers.csv')
data_sheet_Users = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/UniqueUsers.csv', encoding='latin-1')
#print(data_sheet_Users['CreationDate'])
#print(profile_Age)
exit(0)

w, h = 2020, 13
Time = [[0 for x in range(h)] for y in range(w)]

for k in data_sheet_Users.CreationDate:
    date, time = str(k).split(" ")  # Date and time splitting
    y, m, d = date.split("-")
    m = int(m)
    y=int(y)
    Time[y][m]+= 1
profile_Age_byYear = (Time[2008:2020])
print(profile_Age_byYear)

TotalUserPerYear = [0] * 2020

for i in range(2008, 2020):
    for num in Time[i]:
        TotalUserPerYear[i] += num

print("\n\nUser Profile Creation Per Year")
print("\n_____________________________________\n")
for i in range(2008, 2020):
    print("\n{} \t\t\t\t {}".format(i, TotalUserPerYear[i]))

f = open("../../DataSheets/WithBlockchain/Observation/NewUser_PerMonthPerYear.txt","w+")
f.write("\n\nUser Profile Creation in Per Year")
f.write("\n_____________________________________\n")
for i in range(2008,2020):
    if i == 2019:
        f.write("\n{} \t\t\t\t {} (Upto February 5th)".format(i, TotalUserPerYear[i]))
    else:
        f.write("\n{} \t\t\t\t {}".format(i,TotalUserPerYear[i]))
f.close()

years=['Before\n2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019\nUpto February 12']
y_pos=np.arange(len(years))
plt.bar(y_pos,TotalUserPerYear[2007:])
plt.title("Creation of New User Profile Per year")
plt.xticks(y_pos, years)

plt.show()
