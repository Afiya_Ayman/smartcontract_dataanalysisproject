import nltk
from nltk import PorterStemmer, SnowballStemmer
import pandas as pd
import math
from nltk.corpus import stopwords
import gensim
from collections import Counter
import ast

port = PorterStemmer()

df = pd.read_csv('User_TagsFromQandA.csv')#.iloc[:, 1:]  # read in the .csv file

# Tags = df.TagsTheyInteractedWith
# print(df['TagsTheyInteractedWith'])
def unpack_string(row):
    try:
        listOfTuple = ast.literal_eval(row)
        l = []
        for t in listOfTuple:
            l.extend([t[0]] * t[1])
        return l
    except Exception as e:
        print(e)
        return []


df['words'] = df['TagsTheyInteractedWith'].apply(unpack_string)

print(type(df['words']))




word_dict = gensim.corpora.Dictionary(df['words'])
print(word_dict)
exit(0)

count = 0
for k, v in word_dict.items():
    print(k, v)
    count += 1
    if count >= 5:
        break

word_dict.filter_extremes(no_below=15, no_above=0.3)

count = 0
for k, v in word_dict.items():
    print(k, v)
    count += 1
    if count >= 5:
        break

bow_corpus = [word_dict.doc2bow(doc) for doc in df['words']]

from gensim import corpora, models

tfidf = models.TfidfModel(bow_corpus)
corpus_tfidf = tfidf[bow_corpus]
from pprint import pprint

# for idx, doc in enumerate(corpus_tfidf):
#     print(idx)
#     pprint(doc)
#     if idx >= 5:
#         break

lda_model = gensim.models.LdaModel(bow_corpus, num_topics=40, id2word=word_dict, passes=1000, iterations=50000)

for idx, topic in lda_model.print_topics(-1):
    print('Topic: {} Words: {}'.format(idx,topic))

exit(0)
print('-' * 60)
lda_model_tfidf = gensim.models.LdaModel(corpus_tfidf, num_topics=10, id2word=word_dict)
for idx, topic in lda_model_tfidf.print_topics(-1):
    print('Topic: {} Word: {}'.format(idx, topic))
