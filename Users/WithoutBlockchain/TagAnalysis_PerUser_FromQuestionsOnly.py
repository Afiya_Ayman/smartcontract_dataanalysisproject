import pandas as pd
import math
import numpy as np
from matplotlib import pyplot as plt

data_sheet_UserQuestion = pd.read_csv('../../DataCollection/OnlyQuestions_of_Users.csv', encoding='latin-1', low_memory=False)
User=data_sheet_UserQuestion.Id
#print(User[10])
TagsList = data_sheet_UserQuestion.Tags

TagsDictionary = {}

for i in range(0,len(User)):
    key = User[i]
    value = str(TagsList[i])
    if key not in TagsDictionary:
        TagsDictionary[key] = []
    if value == 'nan':
        continue
    value = [s.replace('<', '') for s in value]
    value = [s.replace('>', ',') for s in value]
    value = ''.join(value)
    #print(value)
    TagsDictionary[key].append(value)
#print(TagsDictionary)

for key, value in TagsDictionary.items():
    TagsDictionary[key] = " ".join(TagsDictionary[key])
#print(TagsDictionary)


df = pd.DataFrame.from_dict(TagsDictionary, orient="index")
df.to_csv("TagsPerUserFromQuestion.csv")

df = pd.read_csv('TagsPerUserFromQuestion.csv')
df.columns = ['QuestionID', 'Tags From Question']
df.to_csv("TagsPerUserFromQuestion.csv")





