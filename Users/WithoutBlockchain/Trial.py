import pandas as pd
import operator
from multiprocessing import Pool


def TagsFrequency(j):
    print("$$$$$$$$$$$$$$$$$$")
    TagsFromQuestions = []
    for i in range(len(df_QTags)):
        # print(q[i])
        if QuestionId[i] in j:
            TagsFromQuestions.append(Tags[i])
    TagsFromQuestions = ''.join(TagsFromQuestions).replace("', '", '').split()
    if len(TagsFromQuestions) == 0:
        return ""
    else:
        Tags_User = TagsFromQuestions[0]
        Tags_list = Tags_User.split(",")

        Frequency = dict((i, Tags_list.count(i)) for i in Tags_list)
        sorted_Frequency = sorted(Frequency.items(), key=operator.itemgetter(1), reverse=True)
        print(sorted_Frequency)

        return  str(sorted_Frequency)


df_UserTags = pd.read_csv('TagsPerUser.csv', encoding='latin-1')
print(len(df_UserTags))

df_QTags = pd.read_csv('TagsPerUserFromQuestion.csv', encoding='latin-1')

QuestionId = df_QTags.QuestionID
print(len(QuestionId))

Qid = df_UserTags['Ids of the Questions Answered by User']
Qid = Qid.fillna(0)
print(len(Qid))

q_int = []
q = [str(l).split(' , ') for l in Qid]
for i in range(len(q)):
    for j in range(len(q[i])):
        q[i][j] = int(q[i][j].replace(' ,', ''))

#print(q[i])

Tags = df_QTags['Tags From Question']
print(len(Tags))


new_column = []

# for j in range(len(df_UserTags[:100])):

with Pool(8) as p:
    df_UserTags['TagsFromQuestions'] = p.map(TagsFrequency, q)


#df_UserTags['TagsFromQuestions']=new_column #+ ([""] * (len(df_UserTags) - 100))

df_UserTags.to_csv('TagsPerUser.csv')



