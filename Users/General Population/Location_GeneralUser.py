import pandas as pd
import math
import numpy as np
from geopy.geocoders import Nominatim
from geopy.geocoders import Here
import tqdm
import operator
from multiprocessing import pool, Pool

data_sheet_Users = pd.read_csv('../../DataCollection/GeneralUsers.csv', encoding='latin-1', low_memory=False)

LocationAll = data_sheet_Users.Location
print(LocationAll[2641])
print(type(LocationAll[2641]))
print(len(LocationAll))

#for i in range(len(LocationAll)):
#    data_sheet_Users['Location'] = data_sheet_Users['Location'].replace(np.nan, 'Not Defined')
#print(data_sheet_Users['Location'])

Location = []
for i in range(len(LocationAll)):
    #if type(LocationAll) != float:
    if str(LocationAll[i]) != 'nan':
        Location.append(LocationAll[i])

#Location = sorted(data_sheet_Users.Location, key=str.lower)

print(len(Location))


count = {}
# geolocator = Nominatim(user_agent="specify_your_app_name_here")
geolocator = Here(app_id='i5TJ35mf1B2kxsOdUXXy', app_code='wgDylRoY-fCLShQXIY_06w', timeout=10)

N=0;
for l in tqdm.tqdm(Location):
    N+=1
    if(N%1000==0):
        location = geolocator.geocode(l)
        if location is not None:
            # print(f'{l} ~> {location}')
            country = location.address.split(', ')[-1]
            if country == 'Europe':
                country = location.address.split(', ')[-2]
            if country in count:
                count[country] = count[country] + 1
            else:
                count[country] = 1

#with Pool(8) as p:
    #data_sheet_Users['TagsFromQuestions'] = p.map(Loc_API, Location)


sorted_Country = sorted(count.items(), key=operator.itemgetter(1), reverse=True)
print(sorted_Country)
df = pd.DataFrame(sorted_Country, columns=['Country', 'Users'])
df.to_csv("../../DataSheets/WithoutBlockchain/Observation/GeneralUser_Location_Analysis.csv")
# Location = str.lower(Location)

'''
WC=pd.read_csv('../../DataSheets/WithoutBlockchain/Observation/GeneralUser_Location_Analysis.csv')
print(WC)
y =len(WC)
print(y)
P=0
y=WC.Users
y=list(y)
print(y)
for i in range(len(y)):
    P+=y[i]
    print(P)
print(P)
'''