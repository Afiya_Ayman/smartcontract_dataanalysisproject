import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



data_sheet_Users = pd.read_csv('../../DataCollection/GeneralUsers.csv', encoding='latin-1', low_memory=False)
Reputation = sorted(data_sheet_Users.Reputation, reverse=True)
print(Reputation[:50])
'''
Counter = {x:Reputation.count(x) for x in Reputation}
Counter = Counter.items()
df = pd.DataFrame(Counter, columns=['Reputation','#Users'])
df.to_csv("../DataSheets/Reputation.csv")
'''

ReputationClass = [0]*110 # first 101 index are for 1000 - 99000 [1 thousand per index]
#print(ReputationClass[100])              # the rest 9 index are for 100000 - 900000 [100 thousand per index]
l = 0
h = 999
print(len(Reputation))
for k in range(0,len(ReputationClass)):
    #print(l,h)
    for i in range(0,len(Reputation)):
        if (Reputation[i] >= l and Reputation[i] < h):
            ReputationClass[k] += 1

    if(h<100000):
        l = h + 1
        h = h + 1000
    # else:
    #     l = h + 1
    #     h = h + 100000

#print(ReputationClass)

for k in range(len(ReputationClass)):
    if(k>100):
        ReputationClass[100]= ReputationClass[100]+ReputationClass[k]


f= open("../../DataSheets/WithoutBlockchain/Observation/GeneralUserReputation.txt","w+")
f.write(" Reputation\n (Within )    No of Users")  # table column headings
f.write("\n------ \t -------------")
k = 100000
for i in range(0, len(ReputationClass)):
    #f.write('\n'.format(i))
    if (i >99):
        f.write("\nAbove {} \t\t\t\t {}".format(k, ReputationClass[i]))
        k += 100000
        break
    if(i<100):
        f.write("\n{}000 -- {}000 \t\t\t\t {}".format(i,i+1, ReputationClass[i]))

f.close()


# Labels=[0]*51
# for i in range(1,51):
#     Labels[i]=i*1000
#
# Labels = list(map(str, Labels[1:]))
#
#
# freq_series = pd.Series(ReputationClass[:50])
# #print(freq_series)
#
#
# # Plot the figure.
#
#
#
# plt.figure()
# ax = freq_series.plot(kind='bar')
#
# ax.set_title('Reputation')
# ax.set_xlabel('\nReputation \n (Below Thousands)')
# ax.set_ylabel('Total No of Users = 2643')
# plt.subplots_adjust(bottom=0.25, top=0.9)
#
# ax.set_xticklabels(Labels)
#
# rects = ax.patches
#
# # Make some labels.
#
#
#
# plt.show()
