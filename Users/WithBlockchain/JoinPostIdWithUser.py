import pandas as pd
import csv


data_sheet_UserPosts = pd.read_csv('../../DataCollection/withBlockchain/Posts_of_Blockchain_Users.csv', encoding='latin-1', low_memory=False)
User=data_sheet_UserPosts.OwnerUserId
#print(len(User))

#Preparing ParentId set
ParentIdList = data_sheet_UserPosts.ParentId
ParentIdList = ParentIdList.fillna(0)
ParentIdList = [round(x) for x in ParentIdList]
#print(type(ParentIdList))


parentIdDictionary= {}

for i in range(0, len(User)):
    key = User[i]
    value = str(ParentIdList[i])
    if key not in parentIdDictionary:
        parentIdDictionary[key] = []
    if value == '0':
        continue
    parentIdDictionary[key].append(value)
    parentIdDictionary[key].append(',')
    #set(parentIdDictionary.values())
    #print(parentIdDictionary[key])


for key, value in parentIdDictionary.items():
    parentIdDictionary[key] = " ".join(parentIdDictionary[key])

print(parentIdDictionary)

df = pd.read_csv('TagsPerUser_Blockchain.csv')
UserID = df.UserID
print(UserID)


N = 0

for i in range(len(UserID)):
    N+=1
    print(f"Processing records = {N}")
    for key,value in parentIdDictionary.items():
        #print(value)
        if key == UserID[i]:
            df['Ids of the Questions Answered by User'] = parentIdDictionary.values()
df.to_csv('TagsPerUser_Blockchain.csv')
