import pandas as pd
import math
import numpy as np
from geopy.geocoders import Nominatim
from geopy.geocoders import Here
import tqdm
import operator


data_sheet_Users = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/UniqueUsers.csv', encoding='latin-1')
LocationAll = data_sheet_Users.Location
print(len(LocationAll))

Location = []
N=0
for i in range(len(LocationAll)):
    #if type(LocationAll) != float:
    if str(LocationAll[i]) != 'nan':
        Location.append(LocationAll[i])
    else:
        N+=1

#Location = sorted(data_sheet_Users.Location, key=str.lower)
print(N)
print(len(Location))


count = {}
# geolocator = Nominatim(user_agent="specify_your_app_name_here")
geolocator = Here(app_id='i5TJ35mf1B2kxsOdUXXy', app_code='wgDylRoY-fCLShQXIY_06w',timeout=100)

'''
location = geolocator.geocode('milkyway')
print(location)

location = geolocator.geocode('neverland')
print(location)
for l in Location[:100]:
    location = geolocator.geocode(l)
    if location is not None:
        print(f'{l} ~> {location}')

'''
 
for l in tqdm.tqdm(Location):

    location = geolocator.geocode(l)
    if location is not None:
        # print(f'{l} ~> {location}')
        country = location.address.split(', ')[-1]
        if country == 'Europe':
            country = location.address.split(', ')[-2]
        if country in count:
            count[country] = count[country] + 1
        else:
            count[country] = 1

sorted_Country = sorted(count.items(), key=operator.itemgetter(1),reverse=True)
print(sorted_Country)
df = pd.DataFrame(sorted_Country, columns=['Country','Users'])
df.to_csv("../../DataSheets/WithBlockchain/Observation/Location_Analysis.csv")
#Location = str.lower(Location)
