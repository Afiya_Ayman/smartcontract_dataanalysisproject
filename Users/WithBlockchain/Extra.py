import pandas as pd
import csv
import operator

data_sheet_UserPosts = pd.read_csv('../../DataCollection/withBlockchain/Posts_of_Blockchain_Users.csv',
                                   encoding='latin-1', low_memory=False)
User = data_sheet_UserPosts.OwnerUserId
TagsList = data_sheet_UserPosts.Tags

TagsDictionary = {}

for i in range(0, len(User)):
    key = User[i]
    value = str(TagsList[i])
    if key not in TagsDictionary:
        TagsDictionary[key] = []
    if value == 'nan':
        continue
    value = [s.replace('<', '') for s in value]
    value = [s.replace('>', ',') for s in value]
    value = ''.join(value)
    # print(value)
    TagsDictionary[key].append(value)
# print(TagsDictionary)

for key, value in TagsDictionary.items():
    TagsDictionary[key] = " ".join(TagsDictionary[key])
    # print(TagsDictionary[key])
    #####
    Tags_User = TagsDictionary[key]
    # print(Tags_User)

    Tags_list = Tags_User.split(",")
    # print(Tags_list)
    Frequency = dict((i, Tags_list.count(i)) for i in Tags_list)

    sorted_Frequency = sorted(Frequency.items(), key=operator.itemgetter(1), reverse=True)
    TagsDictionary[key] = str(sorted_Frequency)
    print(TagsDictionary[key])
    #####

df = pd.DataFrame.from_dict(TagsDictionary, orient="index")
df.to_csv("TagsPerUser_Blockchain.csv")
df = pd.read_csv("TagsPerUser_Blockchain.csv")
df.columns = ['UserID', 'Tags They Used']

df.to_csv("TagsPerUser_Blockchain.csv")










