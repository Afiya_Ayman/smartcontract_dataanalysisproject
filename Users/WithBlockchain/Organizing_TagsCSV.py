import pandas as pd
import csv
import operator

# df= pd.read_csv("TagsPerUser_Blockchain.csv")
#
# HEADERS = list(df.head(0))
# print (HEADERS)
#
# df.drop('Unnamed: 0.1', axis = 1, inplace = True)
# df.drop('Unnamed: 0.1.1', axis = 1, inplace = True)
# df.drop('Unnamed: 0.1.1.1', axis = 1, inplace = True)
# df.drop('Unnamed: 0.1.1.1.1', axis = 1, inplace = True)
# df.drop('Unnamed: 0.1.1.1.1.1', axis = 1, inplace = True)
# df.drop('Unnamed: 0.1.1.1.1.1.1', axis = 1, inplace = True)
# df.drop('Unnamed: 0.1.1.1.1.1.1.1', axis = 1, inplace = True)
# df.drop('Tags They Used', axis = 1, inplace = True)
# df.drop('Ids of the Questions Answered by User', axis = 1, inplace = True)
#
#
# df.to_csv('UserTags_for_TopicModelling.csv', index = False)


df= pd.read_csv("UserTags_for_TopicModelling.csv")

HEADERS = list(df.head(0))
print (HEADERS)


#Preparation for Topic Modelling

'''
Tag1= df.TagsFromQuestions
Tag2= df['Tags used by User']
Tag1 = Tag1.fillna(0)
Tag2 = Tag2.fillna(0)

# Tag1=list(Tag1)
# Tag2=list(Tag2)

# print(Tag1[22])

TT=[]
for each in range(0,len(Tag1)):
    if Tag2[each] !="":
        TT.append(str(Tag1[each]) + '' + str(Tag2[each]))
    # TT.append('['+str(Tag1[each]) + '' + str(Tag2[each])+"]")
        TT = [s.replace(", ", ',') for s in TT]
        TT = [s.replace("','", ',') for s in TT]
        TT = [s.replace("0", '') for s in TT]
    # TT = list(TT)


print (len(TT))
print("$$$$$$$$$$$$$$$$$$$$$$$")
# print(TT[3134])

df['TOTAL_TagsTheyInteractedWith'] = TT
#print(df['TagsTheyInteractedWith'])
#
#
df.to_csv('UserTags_for_TopicModelling.csv')
'''

#Tags Frequency

Tags = df.TOTAL_TagsTheyInteractedWith
print(Tags[1])

def TagsFrequency(Tags):
    print("$$$$$$$$$$$$$$$$$$")
    TagsFromQuestions = []
    for i in range(len(Tags)):
        # print(q[i])
        TagsFromQuestions.append(Tags[i])
    TagsFromQuestions = ''.join(TagsFromQuestions).split()
    if len(TagsFromQuestions) == 0:
        return ""
    else:
        Tags_User = TagsFromQuestions[0]
        Tags_list = Tags_User.split(",")
        Frequency = dict((i, Tags_list.count(i)) for i in Tags_list)
        sorted_Frequency = sorted(Frequency.items(), key=operator.itemgetter(1), reverse=True)

        return str(sorted_Frequency)

from multiprocessing import Pool
with Pool(8) as p:
    df['Tags_WithFrequency'] = p.map(TagsFrequency, Tags)
print(df['Tags_WithFrequency'])
df.to_csv('UserTags_for_TopicModelling.csv')


