import pandas as pd
import numpy as np
import matplotlib.pyplot as plt




data_sheet_Users = pd.read_csv('../../DataSheets/WithBlockchain/Dataset/UniqueUsers.csv', encoding='latin-1')
Reputation = sorted(data_sheet_Users.Reputation, reverse=True)
print(Reputation[:100])

ReputationClass = [0]*25 # first 101 index are for 1000 - 99000 [1 thousand per index]
#print(ReputationClass[100])              # the rest 9 index are for 100000 - 900000 [100 thousand per index]
l = 0
h = 999
print(len(Reputation))
for k in range(0,len(ReputationClass)):
    #print(l,h)
    for i in range(0,len(Reputation)):
        if (Reputation[i] >= l and Reputation[i] < h):
            ReputationClass[k] += 1

    if(h<20000):
        l = h + 1
        h = h + 1000
    else:
        l = h + 1
        h = h + 10000

#print(ReputationClass)

for k in range(21,25):
    # if(k>20):
    ReputationClass[20]= ReputationClass[20]+ReputationClass[k]


f= open("../../DataSheets/WithBlockchain/Observation/Reputation_Users.txt","w+")
f.write(" Reputation\n (Within )    No of Users")  # table column headings
f.write("\n------ \t -------------")

k = 20000
for i in range(0, len(ReputationClass)):
    #f.write('\n'.format(i))
    if (i >19):
        f.write("\nAbove {} \t\t\t\t {}".format(k,ReputationClass[i]))
        break
    if(i<=19):
        f.write("\n{}000 -- {}000 \t\t\t\t {}".format(i,i+1, ReputationClass[i]))

f.close()


Labels=[0]*22
for i in range(1,22):
    Labels[i]=i*1000

Labels = list(map(str, Labels[1:]))

freq_series = pd.Series(ReputationClass[:21])


# Plot the figure.

total = len(data_sheet_Users)

plt.figure()
ax = freq_series.plot(kind='bar')

ax.set_title('Reputation')
ax.set_xlabel('\nReputation \n (Below Thousands)')
ax.set_ylabel(f"Total No of Users = {total}")
plt.subplots_adjust(bottom=0.25, top=0.9)

ax.set_xticklabels(Labels)

rects = ax.patches

# Make some labels.



plt.show()
