import csv
import os
import requests
from xml.dom import minidom
import pandas as pd
import xml.etree.ElementTree as ET
import xml.sax
import datetime

csv_columns = ['Id', 'Reputation', 'CreationDate', 'CreationDate', 'DisplayName',
                       'LastAccessDate', 'WebsiteUrl', 'Location', 'AboutMe',
                       'Views','UpVotes', 'DownVotes', 'ProfileImageUrl','AccountId']

df_Users = pd.read_csv('../DataSheets/WithoutBlockchain/Dataset/UniqueUsers.csv', encoding='latin-1')
UserIDlist = df_Users.Id
user_ids = set(UserIDlist)

print(user_ids)


class Posts(xml.sax.ContentHandler):
    def __init__(self):
        self.rows = []
        self.N= 0


    # Call when an element starts
    def startElement(self, tag, attributes):

        if tag == "row":
            if ('Id' in attributes.getNames()):
                if int(attributes.getValue('Id')) not in user_ids:

                    self.N +=1
                    #if (self.N%000 == 0):
                    print(f"Processing {self.N}")

                    fields = []
                    for col in csv_columns:
                        if (col in attributes.getNames()):
                            fields.append(attributes.getValue(col))
                        else:
                            fields.append("")
                    self.rows.append(",".join(['"' + f.replace('"', '""') + '"' for f in fields]))




if (__name__ == "__main__"):
    # create an XMLReader
    parser = xml.sax.make_parser()
    # turn off namepsaces
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # override the default ContextHandler
    Handler = Posts()
    parser.setContentHandler(Handler)

    file= '../DataArchiveStackOverflow/Users.xml'

    parser.parse(file)

    with open('GeneralUsers.csv','w') as fout:
        fout.write(",".join(csv_columns) + "\n")
        for row in Handler.rows:
            fout.write(row + "\n")


