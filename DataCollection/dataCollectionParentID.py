import csv
import os
import requests
from xml.dom import minidom
import pandas as pd
import math
import xml.etree.ElementTree as ET
import xml.sax

def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]

csv_columns = ['Id', 'PostTypeId', 'AcceptedAnswerId', 'CreationDate', 'Score',
                       'ViewCount', 'Body', 'OwnerUserId', 'LastEditorUserId',
                       'LastEditorDisplayName','LastEditDate', 'LastActivityDate', 'Title', 'Tags', 'AnswerCount', 'CommentCount',
                       'FavoriteCount','CommunityOwnedDate','ParentId','OwnerDisplayName', 'ClosedDate']

df = pd.read_csv('Posts_of_Users.csv', encoding='latin-1', low_memory=False)
ParentIDlist = df.ParentId
ParentIDlist = ParentIDlist.fillna(0)
#print(ParentIDlist[:100])
Post_ids = list(ParentIDlist)
Post_ids = [round(x) for x in Post_ids]
Post_ids = remove_values_from_list(Post_ids, 0)
Post_ids=set(Post_ids)
#print(Post_ids)

class Posts(xml.sax.ContentHandler):
    def __init__(self):
        self.rows = []
        self.N = 0


    # Call when an element starts
    def startElement(self, tag, attributes):
        self.N+=1
        if self.N % 1000 == 0:
            print(f"Posts processed so far: {self.N}")
        if tag == "row":
            if ('Id' in attributes.getNames()):
                if int(attributes.getValue('Id')) in Post_ids:
                    fields = []
                    for col in csv_columns:
                        if (col in attributes.getNames()):
                            fields.append(attributes.getValue(col))
                        else:
                            fields.append("")
                    self.rows.append(",".join(['"' + f.replace('"', '""') + '"' for f in fields]))




if (__name__ == "__main__"):
    # create an XMLReader
    parser = xml.sax.make_parser()
    # turn off namepsaces
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # override the default ContextHandler
    Handler = Posts()
    parser.setContentHandler(Handler)

    file= '../DataArchiveStackOverflow/Posts.xml'
    parser.parse(file)

    with open('OnlyQuestions_of_Users.csv','w') as fout:
        fout.write(",".join(csv_columns) + "\n")
        for row in Handler.rows:
            fout.write(row + "\n")
