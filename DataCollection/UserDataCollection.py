import csv
import os
import requests
from xml.dom import minidom
import pandas as pd
import xml.etree.ElementTree as ET
import xml.sax
import datetime

csv_columns = ['Id', 'Reputation', 'CreationDate', 'CreationDate', 'DisplayName',
                       'LastAccessDate', 'WebsiteUrl', 'Location', 'AboutMe',
                       'Views','UpVotes', 'DownVotes', 'ProfileImageUrl','AccountId']

#df_Users = pd.read_csv('../DataSheets/WithoutBlockchain/Dataset/UniqueUsers.csv', encoding='latin-1')
#UserIDlist = df_Users.Id
#user_ids = list(UserIDlist)
Year=['2013',2014,2015,2016,2017,2018]


class Posts(xml.sax.ContentHandler):
    def __init__(self):
        self.rows = []
        self.N= 0


    # Call when an element starts
    def startElement(self, tag, attributes):

        if tag == "row":
            if ('CreationDate' in attributes.getNames()):
                Time = datetime.datetime.strptime(attributes.getValue('CreationDate'), "%Y-%m-%dT%H:%M:%S.%f")
                date, time = str(Time).split(" ")  # Date and time splitting
                y, m, d = date.split("-")
                y = int(y)
                #print(type(y))
                if y >2013:
                # if '2017' in (attributes.getValue('CreationDate')):
                    #print(attributes.getValue('CreationDate'))
                    self.N +=1
                    print(f"Processing {self.N}")

                    fields = []
                    for col in csv_columns:
                        if (col in attributes.getNames()):
                            fields.append(attributes.getValue(col))
                        else:
                            fields.append("")
                    self.rows.append(",".join(['"' + f.replace('"', '""') + '"' for f in fields]))




if (__name__ == "__main__"):
    # create an XMLReader
    parser = xml.sax.make_parser()
    # turn off namepsaces
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # override the default ContextHandler
    Handler = Posts()
    parser.setContentHandler(Handler)

    file= '../DataArchiveStackOverflow/Users.xml'

    parser.parse(file)

    with open('GeneralUsers.csv','w') as fout:
        fout.write(",".join(csv_columns) + "\n")
        for row in Handler.rows:
            fout.write(row + "\n")


