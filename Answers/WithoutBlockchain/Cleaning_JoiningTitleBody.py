import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt

data_sheet_Answers = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/Answers.csv', encoding='latin-1')

AnswerBody_list = data_sheet_Answers.Body
AnswerTitle_list = data_sheet_Answers.Title
#print(AnswerTitle_list)

#Removing Code Snippets
def cleancode(i):
    clean = re.compile('<code>(.|\n|\r\n)*?</code>')
    print(type(clean))
    cleanCode = re.sub(clean, '', i)
    return cleanCode

#Removing HTML Tags
def cleanhtml(X):
    cleanr = re.compile('<.*?>')
    cleanTag = re.sub(cleanr, '', X)

    return cleanTag

AnswerBody_listX=[]

for i in AnswerBody_list:
    X=cleancode(i)
    Y=cleanhtml(X)
    Z=Y.replace('\n',' ')
    W = Z.replace('\r', ' ')
    AnswerBody_listX.append(W)
print(AnswerBody_listX)

# AnswerTitle_listX=[]
#
# for i in AnswerTitle_list:
#     X=cleancode(i)
#     Y=cleanhtml(X)
#     Z=Y.replace('\n',' ')
#     W = Z.replace('\r', ' ')
#     AnswerTitle_listX.append(W)
#
#
# print(AnswerTitle_listX)
# title_Body_Answer=[]
# for each in range(0,len(AnswerBody_listX)):
#     title_Body_Answer.append(AnswerTitle_listX[each] + ' '+str(AnswerBody_listX[each]))

#print(title_Body_Answer)



#for i in AnswerBody_listX:
 #   print ("***********")
  #  print(i)
Word_count_Answers=[]
for i in AnswerBody_listX:
    count = len(re.findall(r'\w+', i))
    Word_count_Answers.append(count)

#print(Word_count_Answers)
data_sheet_Answers['Cleaned_AnswerText'] = AnswerBody_listX
data_sheet_Answers['WordCount_AnswerText'] = Word_count_Answers
data_sheet_Answers.to_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Anwser.csv')

'''
Word_count_Answers=sorted(Word_count_Answers,reverse=True)
#print(type(Word_count_Answers))
#print(Word_count_Answers)

Counter = {x:Word_count_Answers.count(x) for x in Word_count_Answers}

Counter = Counter.items()
data_sheet_Questions = pd.DataFrame(Counter, columns=['No_of_Words_in_Questions','No_of_Posts'])
data_sheet_Questions.to_csv("../../DataSheets/WithoutBlockchain/Observation/WordCount_Questions.csv")
'''

WC = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Question.csv')
#print(WC)


#Plot

# count=WC.WordCount_TitleTextBody
# #print(count)
#
# sizes=[0]*12
#
#
# for i in range(len(count)):
#     if (count[i]<=100):
#         sizes[1] += 1
#     if (count[i]>100 and not(count[i]>200)):
#         sizes[2] += 1
#     if (count[i]>200 and not(count[i]>300)):
#         sizes[3] += 1
#     if (count[i]>300 and not (count[i]>400)):
#         sizes[4] += 1
#     if (count[i]>400 and not (count[i]>500)):
#         sizes[5] += 1
#     if (count[i]>500 and not (count[i]>600)):
#         sizes[6] += 1
#     if (count[i]>600 and not (count[i]>700)):
#         sizes[7] += 1
#     if (count[i]>700 and not (count[i]>800)):
#         sizes[8] += 1
#     if (count[i]>800 and not (count[i]>900)):
#         sizes[9] += 1
#     if (count[i]>900 and not (count[i]>1000)):
#         sizes[10] += 1
#     if (count[i]> 1000):
#         sizes[11] += 1
#
# print(sizes)
#
# No_of_Words =['Less than 100', '100-199', '200-299', '300-399', '400-499', '500-599', '600-699', '700-799', '800-899', '900-999', '1000 or more']
# y_pos=np.arange(len(No_of_Words))
# plt.bar(y_pos, sizes[1:])
# plt.xticks(y_pos, No_of_Words)
# plt.xlabel('No of Words in Questions')
# plt.ylabel('No of Posts')
# plt.title("#Word Count with # Questions")
# plt.show()
#


## Verifying No of Posts
'''
XX=pd.read_csv('DataSheets/CleanedDataset_Question.csv',encoding='latin1')
Question_count = XX.shape[0]
print(Question_count)


y =len(WC)
P=0
for i in range(len(y)):
    if y[i]>0:
        P+=y[i]
print(P)
'''