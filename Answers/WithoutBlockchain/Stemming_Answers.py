import nltk
import csv
from nltk import PorterStemmer
from nltk import SnowballStemmer
from nltk import LancasterStemmer
from nltk import punkt
from collections import defaultdict
import pandas as pd
from nltk.stem import WordNetLemmatizer

# import nltk
# lemma = nltk.wordnet.WordNetLemmatizer()
# lemma.lemmatize('article')
# 'article'
# lemma.lemmatize('leaves')
# 'leaf'


lemmatizer = WordNetLemmatizer()

port = PorterStemmer()

data_sheet_Answers = pd.read_csv('../../DataSheets/WithoutBlockchain/Dataset/CleanedDataset_Anwser.csv')

text_col = data_sheet_Answers['Cleaned_AnswerText']

text_tokenize = text_col.apply(nltk.word_tokenize)
print(text_tokenize[4])

# data_sheet_Questions['Lemmatizer_TextBody'] = text_stem



text_stem = text_tokenize.apply(lambda x: [port.stem(word) for word in x])
print(text_stem[4])

data_sheet_Answers['PorterStemming_TextBody_Answer'] = text_stem
data_sheet_Answers.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Stemming_Answers.csv')

# port = SnowballStemmer("english")
#
# text_stem = text_tokenize.apply(lambda x: [port.stem(word) for word in x])
# #print(text_stem)
# data_sheet_Answers['SnowballStemming_TextBody'] = text_stem
# data_sheet_Answers.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Stemming_Questions.csv')
#
# port = LancasterStemmer()
# text_stem = text_tokenize.apply(lambda x: [port.stem(word) for word in x])
# #print(text_stem)
# data_sheet_Answers['LancasterStemming_TextBody'] = text_stem
# data_sheet_Answers.to_csv('../../DataSheets/WithoutBlockchain/Dataset/Stemming_Questions.csv')